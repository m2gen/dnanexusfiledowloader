# DNANexus File Downloader

---
## Getting Started
---
Instructions to install dependencies and run of ***DnaNexus File Downloader*** python script

### Prerequisites
- Python 3.6.5 (https://www.python.org)
- dx-toolkit 0.240.1+ (https://wiki.dnanexus.com/downloads#Install)
- pip (https://pip.pypa.io/en/stable/installing/)

### Clone Repository
```sh
$ git clone git@bitbucket.org:m2gen/dnanexusfiledowloader.git
```

### Virtual Environment
***DnaNexus File Downloader*** and ***dx-toolkit*** will run only in python version **2.7**. To be able to do this is recommended to create a virtual environment. We're using **pipenv** and these are the following instructions to set up this virtual environment. **Pipenv** requires Python **3.6.x** follow these [instructions](https://www.python.org/downloads/) to install Python.

####Install pipenv
```sh
$ pip install pipenv
```
####Install packages from PipFile
```sh
$ pipenv install
```
### Activate Virtual Environment
Run the following command before login to DNA Nexus platform. 
```sh
$ pipenv shell
```
To exit virtual environment run the following command
```sh
$ exit
```

#### DX-TOOLKIT
Initialize dx environment
```sh
$ source dx-toolkit/environment 
```

Login to DNA Nexus Platform with a token.
```sh
$ dx login --token <TOKEN>
```
Follow these [instructions](https://wiki.dnanexus.com/Command-Line-Client/Login-and-Logout#Authentication-Tokens), to generate the ***token***.

### Running python script
## Arguments

Argument | Description | Required
---------|-------------|--------
-p|Project Name | Yes
-o|Select output directory (default is the root folder of the script)| No
-i|Remote directory| No
-m|Filename filter method. Choices('glob,'exact',regexp').|No
-e|Filename filter expression|No
-r|Process subfolders|No
-l|This will only list dnaNexus objects.|No

## Examples
Here are some examples of how to run the script

## List all files in all folders and subfolders
```sh
$ python app/dnfiles.py -l -r -p <Project Name>
```
## List only the files in the root Project 
```sh
$ python app/dnfiles.py -l -p <Project Name>
```
## List only files with ***.bam*** extension including subfolders
```sh
$ python app/dnfiles.py -l -r -p <Project Name> -m glob -e *.bam
```
## List files from the specified remote folder and subfolders
```sh
$ python app/dnfiles.py -r -p <Project Name> -i Aug_15_2017 -o /myfolder
```

## Download all files in all folders and subfolders (Long Process)
```sh
$ python app/dnfiles.py -r -p <Project Name>
```

## Download only the files in the root Project 
```sh
$ python app/dnfiles.py -p <Project Name>
```

## Download only files with ***.bam*** extension including subfolders
```sh
$ python app/dnfiles.py -r -p <Project Name> -m glob -e *.bam
```

## Download exact filename
```sh
$ python app/dnfiles.py -r -p <Project Name> -m exact -e <filename>
```

## Download files to specified folder (This will download everything)
```sh
$ python app/dnfiles.py -r -p <Project Name> -o /myfolder
```

## Download files from the specified remote folder and subfolders to the specified local folder
```sh
$ python app/dnfiles.py -r -p <Project Name> -i Aug_15_2017 -o /myfolder
```