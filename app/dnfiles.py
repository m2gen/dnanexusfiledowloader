import os
import dxpy
import sys
import argparse
from 	utils.DNFiles 	import *

# Get Script Folder Path
outdir = os.getcwd()

#Define command 
parser = argparse.ArgumentParser(description='Script to downlod DNAnexus files')
parser.add_argument('-p','--project',help='Project name', required=True, type=str)
parser.add_argument('-o','--outdir', help='Select output directory [default: '+outdir+']',default=outdir)
parser.add_argument('-i','--remote', help='Remote directory')
parser.add_argument('-m','--name_mode', help='Filename filter method.', choices=['glob', 'exact', 'regexp'])
parser.add_argument('-e','--expression', help='Filename filter expression', type=str)
parser.add_argument('-r','--recursive',help='Process child folders', action="store_true")
parser.add_argument('-l','--list',help='List DnaNexus object list', action="store_true")


# Assing arguments 
args        = parser.parse_args()
proj_name   = args.project
outdir		  = args.outdir
remotedir   = args.remote

if(args.recursive):
  recursive   = True
else:
  recursive   = False

if args.name_mode is not None:
  if args.expression is None:
    parser.error("--name_mode requires --expression.")

# Seletect project and assing to dn variable
dn = DNFiles(dxpy, proj_name)

#get dict of project names and IDs
pdict = dn.get_all_projects()

#Set root folder for current project
root = '/Avatar_Molecular_Data'

if(remotedir):
  base = root+"/"+remotedir
else:
  base = root

# Get Project ID
if proj_name in pdict.keys():
  projid = pdict[proj_name]
else:
  print("Project name "+proj_name+" does not exist.")
  sys.exit(2)

# Get Files
if args.name_mode is None:
  files = list(dxpy.find_data_objects(classname='file',project=projid,folder=base, describe=True, recurse=recursive))
else:  
  files = list(dxpy.find_data_objects(classname='file',name=args.expression,name_mode=args.name_mode, project=projid,folder=base, describe=True, recurse=recursive))

if(file):
  for file in files:
    if(args.list):
      print(file['describe']['folder']+"/"+file['describe']['name'])
    else:
      targetdir = outdir+file['describe']['folder'].replace(root,"")+"/"
      if not os.path.exists(targetdir):
        os.makedirs(targetdir)

      dxpy.bindings. dxfile_functions.download_dxfile(file['describe']['id'],targetdir+file['describe']['name'],show_progress=True, append=True)
    
  
